# EXO_encounter 667

Install [LÖVE](https://love2d.org) version 11.1 and run `love .` in
this directory.

## Keys

* *arrows*: navigate
* *0-4*: select unit
* *space*: laser
* *period, comma*: aim
* *enter*: dock rover
* *shift*: move/turn slowly
* *tab*: toggle HUD
* *f11*: toggle fullscreen

By [Dan Larkin](https://danlarkin.org/) and [Phil Hagelberg](https://technomancy.us).

## Bugs

* Closing a door while a rover is inside it can push the bug into weird places.
* It won't always autoscroll to units which are along the very bottom.

## Licenses

Original code, prose, and images copyright © 2018 Dan Larkin, Phil
Hagelberg, Zach Hagelberg, and Noah Hagelberg.

Distributed under the GNU General Public License version 3 or later; see file COPYING.

Licensing of third-party art and libraries described in [credits](credits.md)
